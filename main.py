from fastapi import FastAPI
from src.api import trainApi
from src.api import user
from src.api import webhook
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)
app.include_router(trainApi.router,prefix="/train",tags=["train"])
app.include_router(user.router,prefix="/user",tags=["user"])
app.include_router(webhook.router,prefix="",tags=["webhook"])