![Alt text](854.jpg)

```markdown
# LLM Demo App

## Overview

🦙 The Llama Demo App is a FastAPI-based application showcasing the capabilities of Llama, a document storage library for Python. In this demo, users can upload data files (e.g., TXT, DOC) via API and interactively query the uploaded documents using natural language questions. The application utilizes FastAPI for building the API, LangChain for natural language processing, and a vector database for efficient querying.

## Features

- **Upload Data Files:** Users can upload data files in various formats such as TXT or DOC using the provided API endpoint.

- **Ask Questions:** The API includes an endpoint for asking questions about the uploaded documents using natural language. The application leverages LangChain for processing questions and a vector database for efficient querying.

## Technologies Used

- [FastAPI](https://fastapi.tiangolo.com/): A modern, fast (high-performance), web framework for building APIs with Python 3.7+.

- [LangChain](https://github.com/langchain/langchain): A natural language processing library for Python.

- [Llama](https://github.com/dlukes/llama): A document storage library for Python that uses a vector database for fast querying.

- [Vector Database](https://en.wikipedia.org/wiki/Vector_database): A type of database that uses vectorization to store and retrieve data efficiently.

## Setup

1. **Clone the Repository:**
   ```bash
   git clone https://github.com/your-username/llama-demo.git
   cd llama-demo
   ```

2. **Install Dependencies:**
   ```bash
   pip install -r requirements.txt
   ```

3. **Run the Application:**
   ```bash
   uvicorn main:app --reload
   ```
   The application will be accessible at `http://localhost:8000`.

## API Endpoints

### 1. Upload Data File

- **Endpoint:** `/upload-file/`
- **HTTP Method:** POST
- **Request Body:**
  - `file`: The data file to be uploaded (e.g., TXT, DOC).

### 2. Ask Question

- **Endpoint:** `/ask-question/`
- **HTTP Method:** POST
- **Request Body:**
  - `question`: The natural language question to be asked.

## Example Usage

1. **Upload Data File:**
   ```bash
   curl -X POST -F "file=@/path/to/your/file.txt" http://localhost:8000/upload-file/
   ```

2. **Ask Question:**
   ```bash
   curl -X POST -H "Content-Type: application/json" -d '{"question": "What is the demo about?"}' http://localhost:8000/ask-question/
   ```
 