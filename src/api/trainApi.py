from fastapi import APIRouter, File, UploadFile , HTTPException
from src.services.train import TrainModel 
from src.services.fileService import FileService


router = APIRouter()
trainService = TrainModel
fileService = FileService

@router.get("/")
def trainFile():
    return trainService.health()


@router.post("/uploadfile/")
def uploadFile(file: UploadFile = File(...)):
 try:
        result = fileService.upload_file(file)
        if "file_path" in result and result["file_path"]:
            isTrained = trainService.file_data_train_handler()
            print( isTrained)
        else:
            print(result)
        return result
 except Exception as e:
        print(e)
        raise HTTPException(status_code=500, detail=f"Internal Server Error: {str(e)}")
    
 