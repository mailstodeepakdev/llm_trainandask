from fastapi import APIRouter, File, UploadFile , HTTPException
from pydantic import BaseModel
from src.services.train import TrainModel 
from src.services.whatsapp import WhatsApp

router = APIRouter()

trainService = TrainModel
whatsAppService = WhatsApp
class Question(BaseModel):
    question: str
class WhatsAppMessageRequest(BaseModel):
    message: str
    phone: str
    
@router.post("/question")
def askQuestion(data: Question):
    try:
        result = trainService.askQuestion(data.question)
        return {"result": result}
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500, detail=f"Internal Server Error: {str(e)}")
     


@router.post("/whatsapp/message/send")
async def sendMessage(data: WhatsAppMessageRequest):
    try:
        print(data)
        result = await whatsAppService.sendMessage(data.phone ,data.message)
        return {"result": result}
    except Exception as e:
        print(e)
        raise HTTPException(status_code=500, detail=f"Internal Server Error: {str(e)}")
     

