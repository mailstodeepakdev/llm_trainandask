import os
from llama_index import SimpleDirectoryReader, StorageContext, VectorStoreIndex, load_index_from_storage
os.environ["OPENAI_API_KEY"] = "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"
import functools

class TrainModel:
    PERSIST_DIR = "./storage"
    DATA_DIR = "./uploads"

    @classmethod
    def health(cls):
        return "OK"

    @classmethod
    def file_data_train_handler(cls): 
        os.makedirs(cls.PERSIST_DIR, exist_ok=True)
 
        documents = SimpleDirectoryReader(cls.DATA_DIR).load_data()
        index = VectorStoreIndex.from_documents(documents)
        index.storage_context.persist(persist_dir=cls.PERSIST_DIR)

        return True
    
    @staticmethod
    @functools.lru_cache(maxsize=None)
    def askQuestion(query: str):
        print(query)
        storage_context = StorageContext.from_defaults(persist_dir=TrainModel.PERSIST_DIR)
        index = load_index_from_storage(storage_context)

        query_engine = index.as_query_engine()
        response = query_engine.query(query)
        return response.response