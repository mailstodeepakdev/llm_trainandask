import os
from fastapi import UploadFile

class FileService:
    UPLOAD_DIRECTORY = "uploads"

    @staticmethod
    def upload_file(file: UploadFile):
        try:
            os.makedirs(FileService.UPLOAD_DIRECTORY, exist_ok=True)
            file_path = os.path.join(FileService.UPLOAD_DIRECTORY, file.filename)

            with open(file_path, "wb") as file_object:
                file_object.write(file.file.read())

            return {"message": "File uploaded successfully", "file_path": file_path}
        except Exception as e:
            return {"message": f"Error: {str(e)}"}
